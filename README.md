# PPG

# Aim
This is a real time implementation of heart rate (HR) measuring using signal processing.

# Functioning
The program detects the subject's face and forehead (2 regions of interest / ROI). It then uses spatial average and PCA over some time (buffer_duration) to extract the signal of the ROI. It then standardize this signal and determine its fourier transform from which it will determine the frequency of the HR of the subject and display the HR to the user.

The program will run by default for 5 times the buffer duration unless the user changes the value of "NB_OF_SAMPLES" on line 19 in the file "main.cpp". 
At the end of this duration, it will automatically display the average HR over the whole test duration for 2 different ROI: the face and the forehead of the subject. If the user wants to see their instantaneous HR, they can uncomment the lines 380 and 381 of the file "main.cpp".

# Development
This program has been tested on visual studio 2022 on Windows and has been run on ubuntu via the makefile.
